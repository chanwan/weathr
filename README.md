This project is created using [Create React App](https://github.com/facebook/create-react-app), and data taken from [Open Weather Map](https://openweathermap.org/) free API.


## To begin:

After downloaded from git, go into the folder and run the following:

### `npm install`

To install all the necessary dependencies / libraries. Once done, run the next command:

### `npm start`

This will run the app in the development mode.
Open [http://localhost:3310](http://localhost:3310) to view it in the browser.



## To use:

1. At first, you will see a page with only the search bar. 
2. You can begin by typing the city name only `eg., 'Kuala Lumpur'` or city name with country name separated by comma `eg., 'Kuala Lumpur, Malaysia' or 'Kuala Lumpur, MY'`, and hit Enter key or click the Plus button.
3. There is a dropdown to select the refresh interval in seconds for your each of your selected country weather tiles. This refresh interval will run independently for each tiles.
4. Have fun.
