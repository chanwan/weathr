import {GeneralFetchDataType, GeneralObjType, GetWeatherByCityType, GetForecast5DaysByCityType,
    GetForecastHourlyByCityType} from "../models";

const defaultSuccessHandler = (callType: string, response: any) => console.log(`>>> ${callType} Success:`, response);
const defaultErrorHandler = (callType: string, response: any) => console.log(`>>> ${callType} Error:`, response);

type ApiType = GeneralObjType;

const apiService = {
    apiKey: 'c4dcadb6722f0625407f4c0e21274763',
    apiBasePath: 'https://cors-anywhere.herokuapp.com/https://api.openweathermap.org/data/2.5',
    apiProBasePath: 'https://cors-anywhere.herokuapp.com/https://pro.openweathermap.org/data/2.5',
    apiImagePath: (iconStr: string) => `http://openweathermap.org/img/wn/${iconStr}@2x.png`,

    get ApiAddress() {
        return {
            WeatherByCity : this.apiBasePath + `/weather?appid=${this.apiKey}&units=metric&q=`,
            Forecast5DaysByCity : this.apiBasePath + `/forecast?appid=${this.apiKey}&units=metric&q=`,
            ForecastHourlyByCity : this.apiProBasePath + `/forecast/hourly?appid=${this.apiKey}&units=metric&q=`,
        }
    },

    fetchData: (apiPath: string = '', opts: GeneralFetchDataType) => {
        const defaultRequest = {
            method: opts.method || 'GET',
            // credentials: opts.credentials || 'same-origin',
            headers: opts.headers || { 'Content-Type': 'application/json' },
            mode: opts.mode || 'cors',
            cache: opts.cache || 'no-cache',
        };

        const optObj: RequestInit = {
            ...defaultRequest,
            ...(opts.method === 'POST'
                ? { body: JSON.stringify(opts.data) || JSON.stringify({}) }
                : {})
        };

        return (
            fetch(apiPath, optObj)
                .then(response => response.json())
                .then(data => {
                    if(+data.cod === 200) {
                        return data;
                    } else {
                        throw data;
                    }
                })
                .catch(error => ({ error: true, status: error }))
        );
    },

    get ApiCall() {
        return {
            GetWeatherByCity: (opts: GetWeatherByCityType) => {
                const onSuccess = !!opts.onSuccess
                    ? opts.onSuccess
                    : (data: any) => defaultSuccessHandler('GetWeatherByCity', data);

                const onError = !!opts.onError
                    ? opts.onError
                    : (data: any) => defaultErrorHandler('GetWeatherByCity', data);

                this.fetchData(this.ApiAddress.WeatherByCity + opts.searchKey, {
                    data: {},
                }).then((response: any) => !!response.error ? onError(response) : onSuccess(response)
                ).catch((error: any) => onError(error));
                // }).then((response: any) => !!response.error ? onError(mockResp) : onSuccess(mockResp)
                // ).catch((error: any) => onError(mockResp));
            },
            GetForecast5DaysByCity: (opts: GetForecast5DaysByCityType) => {
                const onSuccess = !!opts.onSuccess
                    ? opts.onSuccess
                    : (data: any) => defaultSuccessHandler('GetForecast5DaysByCity', data);

                const onError = !!opts.onError
                    ? opts.onError
                    : (data: any) => defaultErrorHandler('GetForecast5DaysByCity', data);

                this.fetchData(this.ApiAddress.Forecast5DaysByCity + opts.searchKey, {
                    data: {},
                }).then((response: any) => !!response.error ? onError(response) : onSuccess(response)
                ).catch((error: any) => onError(error));
                // }).then((response: any) => !!response.error ? onError(mockResp) : onSuccess(mockResp)
                // ).catch((error: any) => onError(mockResp));
            },
        }
    }
};

const mockResp = {"coord":{"lon":101.69,"lat":3.14},"weather":[{"id":801,"main":"Clouds","description":"few clouds","icon":"02d"}],"base":"stations","main":{"temp":303.39,"feels_like":306.77,"temp_min":302.04,"temp_max":304.26,"pressure":1011,"humidity":70},"visibility":10000,"wind":{"speed":3.6,"deg":60},"clouds":{"all":20},"dt":1581649285,"sys":{"type":1,"id":9427,"country":"MY","sunrise":1581636426,"sunset":1581679680},"timezone":28800,"id":1733046,"name":"Kuala Lumpur","cod":200};


export {
    apiService,
}