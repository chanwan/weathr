import { Preloader } from './preloader';
import * as Logo from './logo.png';

export {
    Preloader,
    Logo,
}