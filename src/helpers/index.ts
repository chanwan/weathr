import {
    RainType,
    SnowType, TileInfoType,
    WeatherInfoType,
    FoundRecordType, ForecastInfoType, DateTimeType
} from "../models";

const checkIfRecordExist = (city: string, country: string, tileList: TileInfoType[]): FoundRecordType => {
    if(tileList.length === 0) {
        return {
            existed: false,
            index: -1
        }
    }

    let foundIndex: number = 0;
    const foundList = tileList.reduce((filtered: any[], tile: TileInfoType, key: number) => {
        if(tile.weatherInfo.cityName === city && tile.weatherInfo.countryName === country) {
            filtered.push(tile);
            foundIndex = key;
        }
        return filtered;
    }, []);

    return {
        existed: foundList.length > 0,
        index: foundIndex
    }
};

const mapDataToWeatherInfo = (data: any): WeatherInfoType => {
    const rainInfo: RainType | null = !!data.rain ? {
        one_hour: data.rain.one_hour,
        three_hour: data.rain.three_hour,
    } : null;

    const snowInfo: SnowType | null = !!data.snow ? {
        one_hour: data.snow.one_hour,
        three_hour: data.snow.three_hour,
    } : null;

    const trimWeatherInfo: WeatherInfoType = {
        cityName: data.name,
        countryName: data.sys.country,
        coord: {
            lon: data.coord.lon,
            lat: data.coord.lat
        },
        weather: {
            id: data.weather[0].id,
            main: data.weather[0].main,
            description: data.weather[0].description,
            icon: data.weather[0].icon,
        },
        main: {
            temp: data.main.temp,
            temp_min: data.main.temp_min,
            temp_max: data.main.temp_max,
            pressure: data.main.pressure,
            humidity: data.main.humidity,
        },
        wind: {
            speed: data.wind.speed,
            deg: data.wind.deg,
        },
        rain: rainInfo,
        snow: snowInfo,
    };

    return trimWeatherInfo;
};

const mapDataToForecastInfo = (data: any): ForecastInfoType => {
    const trimForecastInfo: ForecastInfoType = {
        temperature: data.main.temp,
        weather: {
            id: data.weather[0].id,
            main: data.weather[0].main,
            description: data.weather[0].description,
            icon: data.weather[0].icon,
        },
        time: beautifyDate(data.dt_txt),
    };

    return trimForecastInfo;
};

const beautifyDate = (val: string): DateTimeType => {
    const monthStrArr = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    const tempVal = val.split(' ');

    const dateStr: string = tempVal[0];
    const tempDateStr = dateStr.split('-');

    const yearStr = tempDateStr[0];
    const monthStr = monthStrArr[+tempDateStr[1] - 1];
    const dateNoStr = tempDateStr[2];

    const timeStr: string = tempVal[1];
    const tempTimeStr = timeStr.split(':');
    const hourStr = (+tempTimeStr[0] > 12 ? (+tempTimeStr[0] - 12) : +tempTimeStr[0]);
    const amPmStr = (+tempTimeStr[0] >= 12 ? 'PM' : 'AM');

    return {
        date: `${dateNoStr} ${monthStr}, ${yearStr}`,
        time: +hourStr + amPmStr,
    }
};

const generateArrayRange = (start: number, end: number) => {
    let arr = [];
    while(start <= end) {
        arr.push(start++);
    }
    return arr;
};


export {
    checkIfRecordExist,
    mapDataToWeatherInfo,
    mapDataToForecastInfo,
    generateArrayRange,
}