import React, {useState} from 'react';
import styled from 'styled-components';

import { Preloader as IconPreloader, Logo } from "./assets";
import {apiService} from "./services/api";
import {
    TileInfoType,
    WeatherInfoType,
    FoundRecordType,
    ForecastInfoType
} from './models';
import {
    mapDataToWeatherInfo,
    checkIfRecordExist,
    mapDataToForecastInfo
} from './helpers';
import {
    colors,
    gaps,
    borderRadius,
    elementSizes
} from './constants/layout';
import SearchBar from "./components/SearchBar";
import TileDisplay from "./components/TileDisplay";


let errorMessageTimer: any = null;

const App: React.FC<{}> = () => {
    const initialTile: TileInfoType[] = [];
    const [isLoading, updateLoadStatus] = useState(false);
    const [tileList, updateTiles] = useState(initialTile);
    const [errorMessage, updateErrorMessage] = useState('');

    let selectedRefreshTime: number = 0;

    const onSearchHandler = (searchKey: string) => {
        if(searchKey === '') {
            triggerErrorMessage('Search cannot be empty.');
            return;
        }

        updateLoadStatus(true);

        /** NOTE: Check to see if location already existed after API call,
         * because comparison is dependant on API to return the proper city + country name
         */
        apiService.ApiCall.GetWeatherByCity({
            searchKey,
            onSuccess: onSearchResultSuccess,
            onError: onSearchResultError
        });
    };

    const onSearchResultSuccess = async (data: any) => {
        const mappedWeatherData: WeatherInfoType = mapDataToWeatherInfo(data);

        let newTileList: TileInfoType[] = [];

        const found: FoundRecordType = checkIfRecordExist(mappedWeatherData.cityName, mappedWeatherData.countryName, tileList);

        const hasExisted: boolean = found.existed;
        if(!!hasExisted) {
            updateLoadStatus(false);
            triggerErrorMessage('You have added this city.');
            return;
        } else {
            // NOTE: Get forecast
            const forecastList = await getForecastData(`${mappedWeatherData.cityName}, ${mappedWeatherData.countryName}`);

            const newTileInfo: TileInfoType = {
                refreshTime: selectedRefreshTime,
                weatherInfo: mappedWeatherData,
                forecastInfo: forecastList
            };

            if(tileList.length > 0) {
                newTileList = [
                    ...tileList,
                    newTileInfo
                ];
            } else {
                newTileList.push(newTileInfo);
            }

            updateLoadStatus(false);
            updateTiles(newTileList);
        }
    };

    const onSearchResultError = (data: any) => {
        updateLoadStatus(false);
        triggerErrorMessage('OpenWeather unable to find the city. Please try again.');
    };

    const onRefreshTileHandler = (searchKey: string) => {
        apiService.ApiCall.GetWeatherByCity({
            searchKey,
            onSuccess: onRefreshResultSuccess,
            // onError: onRefreshResultError
        });
    };

    const onRefreshResultSuccess = async (data: any) => {
        const mappedData: WeatherInfoType = mapDataToWeatherInfo(data);
        const found: FoundRecordType = checkIfRecordExist(mappedData.cityName, mappedData.countryName, tileList);
        const index: number = found.index;

        // NOTE: Get forecast
        const forecastList = await getForecastData(`${mappedData.cityName}, ${mappedData.countryName}`);

        const newTileInfo: TileInfoType = {
            refreshTime: tileList[index].refreshTime,
            weatherInfo: mappedData,
            forecastInfo: forecastList,
        };

        const newTileList: TileInfoType[] = tileList;
        newTileList[index] = newTileInfo;

        updateTiles([]);
        updateTiles(newTileList);
    };

    const onRemoveTileHandler = (id: number) => {
        let updatedTileList: TileInfoType[] = tileList.filter((item: TileInfoType, key: number) => (key !== id));
        updateTiles(updatedTileList);
    };

    const onRefreshTimeChangeHandler = (val: number) => {
        selectedRefreshTime = val;
    };

    const triggerErrorMessage = (message: string) => {
        if(!!errorMessageTimer) {
            clearTimeout(errorMessageTimer);
        }

        updateErrorMessage(message);
        errorMessageTimer = setTimeout(() => {
            updateErrorMessage('');
        }, 5000);
    };

    const getForecastData = (searchKey: string): Promise<ForecastInfoType[]> => {
        return new Promise(resolve => {
            apiService.ApiCall.GetForecast5DaysByCity({
                searchKey,
                onSuccess: (forecastData: any) => {
                    const forecastList: ForecastInfoType[] = [];
                    for (let i = 0; i < forecastData.list.length; i++) {
                        const mappedForecastData: ForecastInfoType = mapDataToForecastInfo(forecastData.list[i]);
                        forecastList.push(mappedForecastData);
                    }
                    resolve(forecastList);
                },
                onError: () => {
                    resolve([]);
                }
            });
        });
    };

    return (
        <AppScreenWrapper className="app-screen">
            <HeaderWrapper className="app-header">
                <div className="logo" />
                <div className="search-area">
                    <SearchBar
                        onClick={onSearchHandler}
                        onRefreshTimeChange={onRefreshTimeChangeHandler}
                    />
                </div>
            </HeaderWrapper>

            <MainWrapper className="main-content">
                {!!errorMessage && errorMessage !== '' && (
                    <div className="error-message">
                        <div className="message">{errorMessage}</div>
                    </div>
                )}

                <TileDisplay
                    recordList={tileList}
                    onRemoveTile={onRemoveTileHandler}
                    onRefreshTile={onRefreshTileHandler}
                />
            </MainWrapper>

            { !!isLoading && (
                <div className="overlay">
                    <div className="overlay-content">
                        <IconPreloader />
                    </div>
                </div>
            )}
        </AppScreenWrapper>
    );
};

export default App;


const AppScreenWrapper = styled.div`
    & .overlay {
        position: fixed;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        background: rgba(255,255,255,0.8);
        
        & .overlay-content {
            height: ${elementSizes.ButtonHeight};
            width: ${elementSizes.ButtonHeight};
            
            & > svg {
                height: 100%;
                width: 100%;
            }
        }
    }
`;

const HeaderWrapper = styled.header`
    display: flex;
    background-color: ${colors.GrayLighter};
    
    & > .logo {
        width: ${elementSizes.LogoWidth};
        height: ${elementSizes.LogoWidth};
        background: ${colors.GrayLight} url(${Logo}) center / 90% no-repeat;
    }
    & > .search-area {
        width: calc(100% - ${elementSizes.LogoWidth});
        padding: ${gaps.Large} ${gaps.Common};
        box-sizing: border-box;
    }
`;

const MainWrapper = styled.main`
    
    & .error-message { 
        padding: ${gaps.Common} ${gaps.Common} 0;
        
        & .message {
            padding: ${gaps.Small};
            border: 2px solid ${colors.RedLight};
            box-sizing: border-box;
            color: ${colors.Red};
            text-align: center;
            border-radius: ${borderRadius}px;
            background-color: ${colors.YellowLighter};
        }
    }
`;