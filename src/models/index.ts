export type GeneralObjType = {
    [key: string]: any,
}

export interface GeneralApiCallType {
    onSuccess?: (data: any) => void,
    onError?: (data: any) => void,
}

export interface GetWeatherByCityType extends GeneralApiCallType {
    searchKey: string
}

export interface GetForecast5DaysByCityType extends GeneralApiCallType {
    searchKey: string
}

export interface GetForecastHourlyByCityType extends GeneralApiCallType {
    searchKey: string
}

export interface GeneralFetchDataType extends RequestInit {
    data: any,
}

type CoordinateType = {
    lon: number,
    lat: number,
};

type WeatherType = {
    id: number,
    main: string,
    description: string,
    icon: string,
}

type MainType = {
    temp: number,
    temp_min: number,
    temp_max: number,
    pressure: number,
    humidity: number,
}

type WindType = {
    speed: number,
    deg: number,
}

type CloudType = {
    all: number,
}

export type RainType = {
    one_hour: number,
    three_hour: number,
}

export type SnowType = {
    one_hour: number,
    three_hour: number,
}

export type WeatherInfoType = {
    cityName: string,
    countryName: string,
    coord: CoordinateType,
    weather: WeatherType,
    main: MainType,
    wind: WindType,
    rain?: RainType | null,
    snow?: SnowType | null,
    // cloudiness?: CloudType
}

export type DateTimeType = {
    date: string,
    time: string,
}

export type ForecastInfoType = {
    temperature: number,
    weather: WeatherType,
    time: DateTimeType,
}

export type TileInfoType = {
    refreshTime: number,
    weatherInfo: WeatherInfoType;
    forecastInfo: ForecastInfoType[];
}

export type FoundRecordType = {
    existed: boolean,
    index: number
}