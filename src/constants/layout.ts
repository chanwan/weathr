import {GeneralObjType} from "../models";

type colorType = GeneralObjType;
type mediaBreakpointType = GeneralObjType;
type elementSizeType = GeneralObjType;

const colors: colorType = {
    White: '#FFF',
    Black: '#222',
    GrayDarker: '#666',
    GrayDark: '#9B9095',
    Gray: '#d7d7d7',
    GrayLight: '#e5e5e5',
    GrayLighter: '#f5f5f5',
    BlueDarker: '#307bbb',
    Blue: '#0bbce1',
    BlueLight: '#8bd4ff',
    BlueLighter: '#94ecff',
    Yellow: '#ffe770',
    YellowLighter: '#fff6c9',
    Red: '#ff3a36',
    RedLight: '#ff726f',
};

const mediaBreakpoints: mediaBreakpointType = {
    XLarge: '81.25rem', // 1300px
    Large: '75rem', // 1200px
    Medium: '56.25rem', // 900px
    Small: '37.5rem', // 600px
};

const elementSizes: {[key: string]: string | number}  = {
    LogoWidth: '6.625rem', // 106px

    SearchBarHeight: '2.5rem', // 40px
    SearchBarRefreshTimeWidth: '11rem', // 176px
    SelectorArrowWidth: '2.1rem', // 33.6px

    TileHeaderHeight: '2.5rem', // 40px
    TileTopImageHeight: '7.5rem', // 120px
    TileTopImageWidth: '9.375rem', // 150px
    TimerHeight: '1.7rem', //
    ButtonHeight: '1.875rem', // 30px
};

const gaps: elementSizeType = {
    XSmall: '0.3125rem', // 5px
    Small: '0.625rem', // 10px
    Common: '1rem', // 16px
    Large: '2rem', // 32px
};

const fontSizes: elementSizeType = {
    XSmall: '0.625rem', // 10px
    Small: '0.75rem', // 12px
    Common: '0.875rem', // 14px
    Large: '1rem', // 16px
    XLarge: '1.25rem', // 20px
};


const borderRadius: number = 6;
const boxShadow: string = '0 4px 3px rgba(0, 0, 0, 0.1);';

const commonStyle: GeneralObjType = {
    floatClearStyle: `
        &:after {
        /* used to get auto height with float children */
            display: table;
            clear: both;
            content: '';
        }
    `,

    textOverflowStyle: `
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
    `,

    flexSpreadCenterStyle: `
        display: flex;
        justify-content: space-between;
        align-items: center;
    `,

    blankListStyle: `
        padding: 0;
        margin: 0;
        list-style: none;
    `,

    scaledBGImageStyle: `
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat;
    `,

    absoluteFullStyle: `
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    `,
};


export {
    colors,
    gaps,
    mediaBreakpoints,
    elementSizes,
    fontSizes,

    borderRadius,
    boxShadow,

    commonStyle,
}