import React from "react";
import styled from "styled-components";
import {gaps} from "../../constants/layout";
import {TileInfoType} from "../../models";
import Tile from "../Tile";


type TileDisplayProps = {
    recordList: TileInfoType[],
    onRemoveTile: (val: number) => void,
    onRefreshTile: (val: string) => void,
}

const TileDisplay: React.FC<TileDisplayProps> = ({recordList, onRemoveTile, onRefreshTile}) => {
    const onRemoveHandler = (val: number) => {
        onRemoveTile(val);
    };

    return (
        <TileDisplayWrapper className="tile-display">
            { recordList.map((item: TileInfoType, key: number) => (
                <Tile key={key}
                      id={key}
                      tileInfo={item}
                      onRemove={onRemoveHandler}
                      onRefresh={onRefreshTile}
                />
            ))}
        </TileDisplayWrapper>
    );
};

export default TileDisplay;


const TileDisplayWrapper = styled.div`
    padding: ${gaps.Common} calc(${gaps.Common} / 2);
    box-sizing: border-box;
`;