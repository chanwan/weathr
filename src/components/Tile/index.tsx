import React, {useState} from "react";
import styled from "styled-components";
import { Preloader as IconPreloader } from "../../assets";
import {
    borderRadius, boxShadow,
    colors, commonStyle,
    elementSizes,
    fontSizes,
    gaps, mediaBreakpoints
} from "../../constants/layout";
import {ForecastInfoType, TileInfoType} from "../../models";
import {apiService} from "../../services/api";
import TimerCircle from "../TimerCircle";


type TileProps = {
    tileInfo: TileInfoType,
    onRemove: (keyValue: number) => void,
    onRefresh: (keyValue: string) => void,
    id: number,
};

const Tile: React.FC<TileProps> = ({tileInfo, onRemove, onRefresh, id}) => {
    const [isLoading, updateLoadStatus] = useState(false);

    const onRemoveHandler = (e: React.MouseEvent | React.TouchEvent) => {
        onRemove(id);
    };

    const refreshData = () => {
        updateLoadStatus(true);
        onRefresh(`${tileInfo.weatherInfo.cityName}, ${tileInfo.weatherInfo.countryName}`);

        setTimeout(() => {
            updateLoadStatus(false);
        }, 1000);
    };

    return (
        <TileWrapper className="tile">
            <div className="top">
                <div className="text">
                    <div className="location">
                        <div className="city">{tileInfo.weatherInfo.cityName}</div>
                        <div className="country">{tileInfo.weatherInfo.countryName}</div>
                    </div>
                    <div className="temperature">
                        {tileInfo.weatherInfo.main.temp}<sup>&deg;c</sup>
                    </div>
                </div>
                <div className="image"
                     style={{backgroundImage: `url(${apiService.apiImagePath(tileInfo.weatherInfo.weather.icon)})`}}>
                    <span className="description">{tileInfo.weatherInfo.weather.description}</span>
                </div>
                <div className="timer">
                    {/** NOTE: Use hidden attribute to prevent
                     * 'Warning: Can't perform a React state update on an unmounted component.' issue
                     */}
                    <div className="preloader" hidden={!isLoading}>
                        <IconPreloader />
                    </div>
                    <div className="timer-component" hidden={!!isLoading}>
                        <TimerCircle
                            second={tileInfo.refreshTime}
                            onComplete={refreshData}
                        />
                    </div>
                </div>
            </div>

            <div className="others-properties">
                <ul>
                    <li className="property">
                        <div className="label">Humidity</div>
                        <div className="value">{tileInfo.weatherInfo.main.humidity}%</div>
                    </li>
                    <li className="property">
                        <div className="label">Wind</div>
                        <div className="value">{tileInfo.weatherInfo.wind.speed}m/s</div>
                    </li>
                    <li className="property">
                        <div className="label">Air Pressure</div>
                        <div className="value">{tileInfo.weatherInfo.main.pressure}mm</div>
                    </li>
                </ul>
            </div>


            <div className="forecast">
                <div className="slider">
                    <table>
                        <thead>
                            <tr>{ tileInfo.forecastInfo.map((item: ForecastInfoType, key: number) => (
                                <th key={`h-${key}`}>
                                    <div className="date">{item.time.date}</div>
                                    <div className="time">{item.time.time}</div>
                                </th>
                            ))}</tr>
                        </thead>
                        <tbody>
                            <tr>{ tileInfo.forecastInfo.map((item: ForecastInfoType, key: number) => (
                                <th key={`h-${key}`}>
                                    <div className="image" style={{backgroundImage: `url(${apiService.apiImagePath(item.weather.icon)})`}} />
                                    <div className="temperature">{item.temperature}<sup>&deg;c</sup></div>
                                </th>
                            ))}</tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div className="cta-area">
                <button onClick={onRemoveHandler}>Remove</button>
            </div>
        </TileWrapper>
    );
};

export default Tile;


const TileWrapper = styled.div`
    position: relative;
    display: inline-block;
    width: calc(25% - ${gaps.Common});
    margin: 0 calc(${gaps.Common} / 2) ${gaps.Common};
    box-sizing: border-box;
    border: 1px solid ${colors.BlueLight};
    border-radius: ${borderRadius}px;
    vertical-align: top;
    color: ${colors.White};
    background-color: ${colors.BlueDarker};
    overflow: hidden;
    box-shadow: ${boxShadow};
    
    @media only screen and (max-width: ${mediaBreakpoints.Large}) {
        width: calc(33.33% - ${gaps.Common});
    }
    
    @media only screen and (max-width: ${mediaBreakpoints.Medium}) {
        width: calc(50% - ${gaps.Common});
    }
    
    @media only screen and (max-width: ${mediaBreakpoints.Small}) {
        width: calc(100% - ${gaps.Common});
    }
    
    & .top {
        display: flex;
        min-height: 9rem;
        justify-content: space-between;
        align-items: center;
        border-bottom: 1px solid ${colors.BlueLight};
        background: ${colors.Blue};
        background: linear-gradient(0deg, ${colors.BlueLighter} 0%, ${colors.Blue} 100%);
        
        & > .text {
            width: calc(100% - ${elementSizes.TileTopImageHeight});
            padding: ${gaps.Common} 0 ${gaps.Common} ${gaps.Common};
            box-sizing: border-box;
            
            & .location {  
                display: flex;
                align-items: center;
    
                & .city { 
                    display: inline-block;
                    font-weight: 700;
                    font-size: ${fontSizes.XLarge};
                    color: ${colors.Yellow};
                }
                
                & .country {  
                    display: inline-block;
                    margin-left: ${gaps.XSmall};
                    padding: 3px 5px;
                    border-radius: 2px;
                    font-size: ${fontSizes.Small};
                    color: ${colors.BlueDarker};
                    background-color: ${colors.Yellow};
                }
            }
            & .temperature { 
                font-weight: 700;
                font-size: 3rem;
                letter-spacing: -0.04em;
                white-space: nowrap;
                
                & > sup {
                    display: inline-block;
                    vertical-align: top;
                }
            }
        }
        
        & > .image {
            position: relative;
            width: ${elementSizes.TileTopImageWidth};
            height: ${elementSizes.TileTopImageHeight};
            background: center / contain no-repeat;
            
            & > .description {
                position: absolute;
                left: 0;
                bottom: ${gaps.Small};
                display: block;
                width: 100%;
                color: ${colors.BlueDarker};
                font-size: ${fontSizes.Small};
                text-align: center;
            }
        }
        
        & > .timer {
            position: absolute;
            width: ${elementSizes.TimerHeight};
            height: ${elementSizes.TimerHeight};
            top: ${gaps.XSmall};
            right: ${gaps.XSmall};
            
            & .timer-component {
                width: 100%;
                height: 100%;
            }
            
            & .preloader,
            & .preloader > svg {
                width: 100%;
                height: 100%;
            }
        }
    }
    
    & .others-properties {
        padding: ${gaps.Common};
        box-sizing: border-box;
        border-bottom: 1px solid ${colors.BlueLight};
        
        & > ul {
            ${commonStyle.blankListStyle}        
            display: flex;
            width: 100%;
            justify-content: space-between;
            
            & li.property {
                text-align: center;
                
                & .label { 
                    font-weight: 500;
                    font-size: ${fontSizes.Small};
                }
                
                & .value {  
                    font-weight: 700;
                }
            }
        }
    }
    
    & .forecast {
        padding: ${gaps.Common};
        box-sizing: border-box;
        border-bottom: 1px solid ${colors.BlueLight};
        
        & .slider {
            width: 100%;
            overflow-x: auto;
            
            & > table {
                & tr {
                    & th {
                        font-weight: 400;
                        padding: 0 ${gaps.Common} ${gaps.Small} 0;
                    }
                    
                    & td {
                        padding: 0 ${gaps.Small} 0 0;
                    }
                }
                
                & .date {
                    font-weight: 400;
                    white-space: nowrap;
                }
                
                & .time {
                    font-weight: 700;
                    font-size: ${fontSizes.Large};
                }
                
                & .image {
                    width: 100%;
                    height: 4rem;
                    background: center / 110% no-repeat;
                }
                
                & .temperature {
                    font-weight: 700;
                    font-size: ${fontSizes.XLarge};
                    white-space: nowrap;
                }
            }
        }
    }
    
    & .cta-area {
        padding: ${gaps.Common};
        box-sizing: border-box;
        border-bottom: 1px solid ${colors.BlueLight};
        text-align: right;
    
        & > button {
            height: ${elementSizes.ButtonHeight};
            padding: 0 ${gaps.Common};
            font-size: ${fontSizes.Small};
            border: 0;
            border-radius: ${borderRadius}px;
            background-color: ${colors.White};
            cursor: pointer;
            
            &:focus {
                outline: none;
            }
        }
    }
`;