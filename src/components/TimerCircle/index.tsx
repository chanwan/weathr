import React, {useEffect, useState} from "react";
import styled from "styled-components";
import {colors, elementSizes} from "../../constants/layout";
import {generateArrayRange} from "../../helpers";


type TimerCircleProps = {
    second: number,
    onComplete: () => void,
}

const TimerCircle: React.FC<TimerCircleProps> = ({second, onComplete}) => {
    let isMounted: boolean = true;

    const [curTime, updateCurTime] = useState(second);
    const [percentage, updatePercentage] = useState(100);

    let timer: any = null;
    timer = setTimeout(() => {
        if (curTime > 0) {
            updateCurTime(() => curTime - 1);
            updatePercentage(Math.floor(100 / second * (curTime - 1)));
        } else {
            if (!!timer) clearTimeout(timer);
        }
    }, 1000);

    useEffect(() => {
        if(curTime === 0) {
            triggerApi();
        }

        return () => {
            isMounted = false;
        };
    }, [curTime]);

    const triggerApi = () => {
        onComplete();
    };

    return (
        <TimerCircleWrapper className="timer-circle">
            <div className={`c100 p${percentage} blue`}>
                <span>{curTime}</span>
                <div className="slice"
                     style={{ ...(percentage > 50 ? {clip: `rect(auto, auto, auto, auto)`} : {}) }}>
                    <div className="bar" style={{transform: `rotate(${percentage*segmentDegValue}deg)`}} />
                    <div className="fill" />
                </div>
            </div>
        </TimerCircleWrapper>
    );
};

export default TimerCircle;

const segmentDegValue: number = 3.6;
const half50Arr: number[] = generateArrayRange(51, 100);

const fillStyle = `
    position: absolute;
    border: 0.14em solid ${colors.BlueDarker};
    width: 0.72em;
    height: 0.72em;
    clip: rect(0em, 0.5em, 1em, 0em);
    border-radius: 50%;
    transform: rotate(0deg);
`;

const TimerCircleWrapper = styled.div`
    width: 100%;
    height: 100%;
    
    .rect-auto {
        clip: rect(auto, auto, auto, auto);
    }
    
    .pie {
        ${fillStyle};
    }
    
    .pie-fill {
        transform: rotate(180deg);
    }
    
    & .c100 {
        position: relative;
        font-size: ${elementSizes.TimerHeight};
        width: 1em;
        height: 1em;
        border-radius: 50%;
        
        & *, 
        & *:before, 
        & *:after {
            box-sizing: content-box;
        }
        
        & > span {
            position: absolute;
            left: 0;
            top: 0;
            display: flex;
            width: 100%;
            height: 100%;
            font-size: 0.5em;
            justify-content: center;
            align-items: center;
            z-index: 1;
            transition-property: all;
            transition-duration: 0.2s;
            transition-timing-function: ease-out;
        }
        
        &:after {
            position: absolute;
            top: 0.14em;
            left: 0.14em;
            display: block;
            content: " ";
            border-radius: 50%;
            width: 0.72em;
            height: 0.72em;
            transition-property: all;
            transition-duration: 0.2s;
            transition-timing-function: ease-in;
        }
        
        & .slice {
            position: absolute;
            width: 1em;
            height: 1em;
            clip: rect(0em, 1em, 1em, 0.5em);
        }
        
        & .bar {
            ${fillStyle}
        } 
        
        ${ half50Arr.map((item: number) => (`
        &.p${item} {
            & .fill {
                ${fillStyle}
                transform: rotate(180deg);
            }
            
            & .bar:after {
                transform: rotate(180deg);
            } 
        }
        `))}
    }        
`;
