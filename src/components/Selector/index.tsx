import React, {useState, useEffect} from "react";
import styled from "styled-components";
import {
    elementSizes,
    gaps,
    commonStyle
} from "../../constants/layout";


type SelectorProps = {
    optionList: number[],
    onChange: (value: number) => void,
}

const Selector: React.FC<SelectorProps> = ({optionList, onChange}) => {
    const initialTime: number = optionList[0];

    const [refreshTime, updateRefreshTime] = useState(initialTime);

    const onChangeHandler = (e: React.ChangeEvent<HTMLSelectElement>) => {
        updateRefreshTime(Number(e.target.value));
    };

    /** NOTE:
     * technically, I can just put this in the onChangeHandler
     * but by using useEffect, the moment Selector is mounted,
     * I can pass initial value to parent component without extra effort
     * */
    useEffect(() => {
        onChange(refreshTime);
    }, [onChange, refreshTime]);

    return (
        <SelectorWrapper className="selector">
            <div id="selectedText">Refresh every {refreshTime}s</div>
            <div className="arrow">
                <i className="material-icons">keyboard_arrow_down</i>
            </div>
            <select
                name="refreshTime"
                id="refreshTime"
                onChange={onChangeHandler}
            >
                { optionList.map((item: number, key: number) => (
                    <option key={key} value={item}>{item}</option>
                ))}
            </select>
        </SelectorWrapper>
    );
};

export default Selector;


const SelectorWrapper = styled.div`
    ${commonStyle.absoluteFullStyle}
    cursor: pointer;
    
    & > select {
        ${commonStyle.absoluteFullStyle}
        opacity: 0;
        cursor: pointer;
    }
    
    & #selectedText, 
    & .arrow {
        position: absolute;
        top: 0;
        height: 100%;
        line-height: ${elementSizes.SearchBarHeight};
        box-sizing: border-box;
    }
    
    & #selectedText {
        left: 0;
        width: calc(100% - ${elementSizes.SelectorArrowWidth});
        padding-left: ${gaps.Common};
    }
    
    & .arrow {
        display: flex;
        align-items: center;
        right: 0;
        width: ${elementSizes.SelectorArrowWidth};
        padding-right: ${gaps.Small};
    }
`;