import React from "react";
import styled from "styled-components";
import {
    borderRadius,
    colors,
    elementSizes,
    fontSizes,
    gaps
} from "../../constants/layout";
import {refreshTimeList} from "../../constants/app";
import Selector from "../Selector";


type SearchBarProps = {
    onClick: (val: string) => void
    onRefreshTimeChange: (val: number) => void
}

const SearchBar: React.FC<SearchBarProps> = ({onClick, onRefreshTimeChange}) => {
    let searchKey: string = '';
    const inputRef: React.RefObject<HTMLInputElement> = React.createRef();

    const onClickHandler = (e: React.MouseEvent | React.TouchEvent) => {
        inputRef.current!.value = '';
        onClick(searchKey);
    };

    const onKeyUpHandler = (e: React.KeyboardEvent) => {
        const target = e.target as HTMLInputElement;
        searchKey = target.value;
    };

    const onKeyPressHandler = (e: React.KeyboardEvent) => {
        const code = (e.keyCode ? e.keyCode : e.which);
        if(code === 13) { // NOTE: Enter keycode
            inputRef.current!.value = '';
            onClick(searchKey);
        }
    };

    return (
        <SearchBarWrapper className="search-bar">
            <div className="input-holder">
                <input
                    ref={inputRef}
                    type="text"
                    className="search"
                    placeholder={'City name, Country'}
                    onKeyUp={onKeyUpHandler}
                    onKeyPress={onKeyPressHandler}
                />
                <button className="search-button" onClick={onClickHandler}>
                    <i className="material-icons">add</i>
                </button>
                <div className="refresh-time">
                    <Selector
                        optionList={refreshTimeList}
                        onChange={onRefreshTimeChange}
                    />
                </div>
            </div>
        </SearchBarWrapper>
    );
};

export default SearchBar;


const SearchBarWrapper = styled.div`    
    & .input-holder {
        height: ${elementSizes.SearchBarHeight};
        background-color: ${colors.White};
        border: 1px solid ${colors.BlueLight};
        border-radius: ${borderRadius}px;
        
        & input[type="text"] {
            float: left;
            width: calc(100% - ${elementSizes.SearchBarHeight} - ${elementSizes.SearchBarRefreshTimeWidth});
            padding: 0 ${gaps.Common};
            line-height: ${elementSizes.SearchBarHeight};
            border: 0;
            box-sizing: border-box;
            background: transparent;
            font-size: ${fontSizes.Common};
            
            &:focus {
                outline: none;
            }
        }
        
        & .refresh-time {
            position: relative;
            float: right;
            height: ${elementSizes.SearchBarHeight};
            width: ${elementSizes.SearchBarRefreshTimeWidth};
            border-left: 1px solid ${colors.BlueLight};
            border-right: 1px solid ${colors.BlueLight};
            box-sizing: border-box;
        }
        
        & .search-button {
            float: right;
            height: ${elementSizes.SearchBarHeight};
            width: ${elementSizes.SearchBarHeight};
            border: 0;
            background: ${colors.BlueLight};
            color: ${colors.White};
            
            &:focus {
                outline: none;
            }
        }
    }
`;